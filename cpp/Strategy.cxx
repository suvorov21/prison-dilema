//
// Created by SERGEY SUVOROV on 25/10/2022.
//

#include "Strategy.hxx"


Actions AlwaysSilent::decision(const history& opponent, const history& mine) {
    return Actions::silent;
}

Actions AlwaysBetray::decision(const history& opponent, const history& mine) {
    return Actions::betray;
}

Actions Random::decision(const history& opponent, const history& mine) {
    return static_cast<Actions>(distrib(gen));
}

Actions OneRevenge::decision(const history& opponent, const history& mine) {
    if (opponent.empty()) {
        return Actions::silent;
    }
    return opponent.back();
}

Actions RandomRevenge::decision(const history& opponent, const history& mine) {
    if (!opponent.empty() && opponent.back() == Actions::betray) {
        return Actions::betray;
    }
    return static_cast<Actions>(distrib(gen));
}

Actions TwoRevenge::decision(const history& opponent, const history& mine) {
    if (opponent.empty()) {
        return Actions::silent;
    }
    if (opponent.back() == Actions::betray) {
        return Actions::betray;
    }

    if (opponent.size() > 2 && opponent[opponent.size() - 2] == Actions::betray) {
        return Actions::betray;
    }
    return Actions::silent;
}

Actions OneRevengeForgive::decision(const history& opponent, const history& mine) {
    if (opponent.empty()) {
        return Actions::silent;
    }
    if (mine.back() == Actions::betray) {
        return Actions::silent;
    }
    return opponent.back();
}

Actions InfiniteRevenge::decision(const history& opponent, const history& mine) {
    if (opponent.empty()) {
        return Actions::silent;
    }
    if (mine.back() == Actions::betray) {
        return Actions::betray;
    }
    return opponent.back();
}