//
// Created by SERGEY SUVOROV on 25/10/2022.
//

#include "Game.hxx"

#include <iostream>

void Game::duel(const int lhsIndex, const int rhsIndex) {
    _lhsActions.clear();
    _rhsActions.clear();

    int lhsDuelScore{0};
    int rhsDuelScore{0};
    Result result;
    for (int round = 0; round < _rounds; ++round) {
        auto lhsChoice = _strategies[lhsIndex]->decision(_rhsActions, _lhsActions);
        auto rhsChoice = _strategies[rhsIndex]->decision(_lhsActions, _rhsActions);

        result = fight(lhsChoice, rhsChoice);

        _lhsActions.emplace_back(lhsChoice);
        _rhsActions.emplace_back(rhsChoice);

        _scoresAbsolute[lhsIndex] += static_cast<int>(result.first);
        _scoresAbsolute[rhsIndex] += static_cast<int>(result.second);

        lhsDuelScore += static_cast<int>(result.first);
        rhsDuelScore += static_cast<int>(result.second);
    } // over rounds

    std::cout << _strategies[lhsIndex]->_name << " vs " << _strategies[rhsIndex]->_name << std::endl;
    std::cout << lhsDuelScore << "\t" << rhsDuelScore << std::endl;

    if (lhsDuelScore < rhsDuelScore) {
        _scoresWins[lhsIndex] += 1;
    } else if (lhsDuelScore > rhsDuelScore) {
        _scoresWins[rhsIndex] += 1;
    } else {
        _scoresDraws[lhsIndex] += 1;
        _scoresDraws[rhsIndex] += 1;
    }
}

Result Game::fight(const Actions lhsChoice, const Actions rhsChoice) {
    if (lhsChoice == rhsChoice) {
        if (lhsChoice == Actions::betray) {
            return {Scores::bothBetray, Scores::bothBetray};
        } else {
            return {Scores::bothSilent, Scores::bothSilent};
        }
    } else {
        if (lhsChoice == Actions::betray) {
            return {Scores::betrayAgainstSilent, Scores::silentAgainstBetray};
        } else {
            return {Scores::silentAgainstBetray, Scores::betrayAgainstSilent};
        }
    }
}

void Game::simulate() {
    _scoresAbsolute.clear();
    _scoresAbsolute.resize(_strategies.size(), 0);

    _scoresWins.clear();
    _scoresWins.resize(_strategies.size(), 0);

    _scoresDraws.clear();
    _scoresDraws.resize(_strategies.size(), 0);

    for (auto lhsIndex = 0; lhsIndex < _strategies.size(); ++lhsIndex) {
        for (auto rhsIndex = lhsIndex; rhsIndex < _strategies.size(); ++rhsIndex) {
            duel(lhsIndex, rhsIndex);
        }
    }

    dumpResults();
}

void Game::dumpResults() {
    std::cout << "       Game Results       " << std::endl;
    std::cout << "Strategy name\tAbs score\t\t\tWin\tDraws\n";
    for (auto index = 0; index < _strategies.size(); ++index) {
        std::cout << _strategies[index]->_name << "\t"
                  << _scoresAbsolute[index] << "\t\t\t\t"
                  << _scoresWins[index] << "\t"
                  << _scoresDraws[index] << std::endl;
    }
}
