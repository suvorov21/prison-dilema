//
// Created by SERGEY SUVOROV on 25/10/2022.
//

#ifndef PRISON_GAME_CPP_GAME_HXX_
#define PRISON_GAME_CPP_GAME_HXX_

#include "Strategy.hxx"

class Game;

enum class Scores {
    bothBetray = 8,
    bothSilent = 4,
    betrayAgainstSilent = 0,
    silentAgainstBetray = 10
};

using Result = std::pair<Scores, Scores>;

class Game {
    std::vector<Strategy_ptr> _strategies;
    /// Absolute amount of penalties
    std::vector<int> _scoresAbsolute;
    /// number of wins
    std::vector<int> _scoresWins;
    /// number of draws
    std::vector<int> _scoresDraws;
    int _rounds{100};

    std::vector<Actions> _lhsActions;
    std::vector<Actions> _rhsActions;

    static Result fight(Actions lhsChoice, Actions rhsChoice);
    /// Round within 2 strategies from _strategies
    void duel(int lhsIndex, int rhsIndex);
 public:
    void addStrategy(Strategy_ptr ptr) {_strategies.emplace_back(std::move(ptr));}
    void setRounds(const int r) {_rounds = r;}

    /// Simulate the tournament across all strategies in _strategies
    void simulate();

    /// Print tournament results
    void dumpResults();
};

#endif //PRISON_GAME_CPP_GAME_HXX_
