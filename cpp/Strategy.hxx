//
// Created by SERGEY SUVOROV on 25/10/2022.
//

#ifndef PRISON_GAME_CPP_STRATEGY_HXX_
#define PRISON_GAME_CPP_STRATEGY_HXX_

#include <memory>
#include <vector>
#include <string>
#include <random>

enum class Actions {
    betray = 0,
    silent = 1
};

using history = std::vector<Actions>;

class Strategy;
using Strategy_ptr = std::unique_ptr<Strategy>;

class Strategy {
 public:
    explicit Strategy(std::string name) : _name(std::move(name)) {}
    virtual ~Strategy() = default;
    std::string _name;
    virtual Actions decision(const history& opponent, const history& mine) = 0;
};



class AlwaysSilent : public Strategy {
 public:
    AlwaysSilent() : Strategy("Always silent") {}
    ~AlwaysSilent() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};


class AlwaysBetray : public Strategy {
 public:
    AlwaysBetray() : Strategy("Always betray") {}
    ~AlwaysBetray() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class Random : virtual public Strategy {
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> distrib;
 public:
    Random() : Strategy("Random choice"), gen(rd()), distrib(0, 1) {}
    ~Random() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class OneRevenge : public Strategy {
 public:
    OneRevenge() : Strategy("One revenge  ") {}
    ~OneRevenge() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class TwoRevenge : public Strategy {
 public:
    TwoRevenge() : Strategy("Two revenges ") {}
    ~TwoRevenge() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class OneRevengeForgive : public Strategy {
 public:
    OneRevengeForgive() : Strategy("One revenge fg") {}
    ~OneRevengeForgive() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class RandomRevenge : virtual public Strategy, Random {
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> distrib;
 public:
    RandomRevenge() : Strategy("Random+revenge"), Random() {}
    ~RandomRevenge() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

class InfiniteRevenge : public Strategy {
 public:
    InfiniteRevenge() : Strategy("Infin revenge") {}
    ~InfiniteRevenge() override = default;
    Actions decision(const history& opponent, const history& mine) override;
};

#endif //PRISON_GAME_CPP_STRATEGY_HXX_
