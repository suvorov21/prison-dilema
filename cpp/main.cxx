#include "Game.hxx"

int main() {
    Game game;
    game.addStrategy(std::make_unique<AlwaysSilent>());
    game.addStrategy(std::make_unique<AlwaysBetray>());
    game.addStrategy(std::make_unique<Random>());
    game.addStrategy(std::make_unique<OneRevenge>());
    game.addStrategy(std::make_unique<TwoRevenge>());
    game.addStrategy(std::make_unique<OneRevengeForgive>());
    game.addStrategy(std::make_unique<RandomRevenge>());
    game.addStrategy(std::make_unique<InfiniteRevenge>());

    game.setRounds(1000);
    game.simulate();
}
