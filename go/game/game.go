package game

import (
	"fmt"
	strategy "prison/strategy"
)

type Scores int

const (
	bothBetray          Scores = 8
	bothSilent                 = 4
	betrayAgainstSilent        = 0
	silentAgainstBetray        = 10
)

type Result struct {
	first, second Scores
}

type Game struct {
	Rounds        int
	strategies    []strategy.StrategyPlayer
	scoreAbsolute []int
	scoreWins     []int
	scoreDrawn    []int
}

func (g *Game) AddStrategy(s strategy.StrategyPlayer) {
	g.strategies = append(g.strategies, s)
	g.scoreAbsolute = append(g.scoreAbsolute, 0)
	g.scoreWins = append(g.scoreWins, 0)
	g.scoreDrawn = append(g.scoreDrawn, 0)
}

func (g *Game) fight(lhsChoice, rhsChoice strategy.Actions) Result {
	if lhsChoice == rhsChoice {
		if lhsChoice == strategy.Betray {
			return Result{bothBetray, bothBetray}
		} else {
			return Result{bothSilent, bothSilent}
		}
	} else {
		if lhsChoice == strategy.Betray {
			return Result{betrayAgainstSilent, silentAgainstBetray}
		} else {
			return Result{silentAgainstBetray, betrayAgainstSilent}
		}
	}
}

func (g *Game) duel(lhsIndex, rhsIndex int) {
	lhsActions := make([]strategy.Actions, len(g.strategies))
	rhsActions := make([]strategy.Actions, len(g.strategies))

	lhsDuelScore, rhsDuelScore := 0, 0

	for i := 0; i < g.Rounds; i++ {
		lhsChoice, rhsChoice := g.strategies[lhsIndex].Decision(lhsActions, rhsActions),
			g.strategies[rhsIndex].Decision(lhsActions, rhsActions)
		result := g.fight(lhsChoice, rhsChoice)

		lhsActions = append(lhsActions, lhsChoice)
		rhsActions = append(rhsActions, rhsChoice)

		g.scoreAbsolute[lhsIndex] += int(result.first)
		g.scoreAbsolute[rhsIndex] += int(result.second)

		lhsDuelScore += int(result.first)
		rhsDuelScore += int(result.second)
	} // over rounds

	fmt.Printf("%v vs. %v\n%d\t%d\n",
		g.strategies[lhsIndex].Name(),
		g.strategies[rhsIndex].Name(),
		lhsDuelScore,
		rhsDuelScore)

	if lhsDuelScore < rhsDuelScore {
		g.scoreWins[lhsIndex] += 1
	} else if lhsDuelScore > rhsDuelScore {
		g.scoreWins[rhsIndex] += 1
	} else {
		g.scoreDrawn[lhsIndex] += 1
		g.scoreDrawn[rhsIndex] += 1
	}
}

func (g *Game) Simulate() {
	for lhsIndex := 0; lhsIndex < len(g.strategies); lhsIndex++ {
		for rhsIndex := lhsIndex; rhsIndex < len(g.strategies); rhsIndex++ {
			g.duel(lhsIndex, rhsIndex)
		}
	}

	g.DumpResults()
}

func (g *Game) DumpResults() {
	fmt.Printf("Game results:\n\n")
	fmt.Printf("Strategy name\tAbs score\t\tWin\tDraws\n\n")
	for i := 0; i < len(g.strategies); i++ {
		fmt.Printf("%v\t%d\t\t\t%d\t%d\n",
			g.strategies[i].Name(),
			g.scoreAbsolute[i],
			g.scoreWins[i],
			g.scoreDrawn[i])
	}
}
