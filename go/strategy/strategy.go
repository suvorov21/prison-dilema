package strategy

type Actions int

const (
	Betray Actions = 0
	Silent         = 1
)

type History []Actions

// strategy interface definition
type StrategyPlayer interface {
	Decision(opponent, mine History) Actions
	Name() string
}

type Strategy struct {
	name string
}

func (s Strategy) Name() string { return s.name }

// stragetgy implementation
type alwaysSilent struct {
	Strategy
}

func (alwaysSilent) Decision(opponent, mine History) Actions {
	return Silent
}

func NewAlwaysSilent() StrategyPlayer {
	strat := alwaysSilent{Strategy: Strategy{"Always silent"}}
	return StrategyPlayer(strat)
}

// betray strategy impl
type alwaysBetray struct {
	Strategy
}

func (alwaysBetray) Decision(opponent, mine History) Actions {
	return Betray
}

func NewAlwaysBetray() StrategyPlayer {
	strat := alwaysBetray{Strategy: Strategy{"Always betray"}}
	return StrategyPlayer(strat)
}

// One revenge strategy
type oneRevenge struct {
	Strategy
}

func (oneRevenge) Decision(opponent, mine History) Actions {
	if len(opponent) == 0 {
		return Silent
	}

	if mine[len(opponent)-1] == Betray {
		return Silent
	}
	return opponent[len(opponent)-1]
}

func NewOneRevenge() StrategyPlayer {
	strat := oneRevenge{Strategy: Strategy{"One revenge"}}
	return StrategyPlayer(strat)
}
