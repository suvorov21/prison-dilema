package main

import (
	game "prison/game"
	strategy "prison/strategy"
)

func main() {
	game := game.Game{Rounds: 1000}

	game.AddStrategy(strategy.NewAlwaysSilent())
	game.AddStrategy(strategy.NewAlwaysBetray())
	game.AddStrategy(strategy.NewOneRevenge())

	game.Simulate()
}
